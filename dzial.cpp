/*
    Dodawanie i odejmowanie BIGNUM
    Copyright (C) 2019 Piotr Kubala

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <bits/stdc++.h>

using namespace std;

struct Licz
{
    string cyf;//zapisane najwyzszy indeks najstarsza cyfra, ASCII - '0'
    bool znak;//1 -> -, 0 -> +
    int dl;
    Licz(string);
};

bool mniej_bezw(Licz &, Licz &);//|a| < |b|
bool mniej(Licz &, Licz &);//a < b
void dodaj_dod(Licz &, Licz &, Licz &);//a + b = c, wszystkie nieujemne
void odejmij_dod(Licz &, Licz &, Licz &);//a - b = c, wszystkie nieujemne, a >= b
void dodaj(Licz &, Licz &, Licz &);//a + b = c
void odejmij(Licz &, Licz &, Licz &);//a - b = c

int main()
{
    string a, b;
    cin >> a >> b;
    Licz la(a), lb(b), lc("0");
    odejmij(la, lb, lc);
    if(lc.znak)
	cout << '-';
    for(int i = lc.dl - 1; i >= 0; i--)
	cout << static_cast <char> (lc.cyf[i] + '0');
    cout << '\n';
    return 0;
}

bool mniej_bezw(Licz &a, Licz &b)
{
    if(a.dl != b.dl)
	return a.dl < b.dl;
    for(int i = a.dl - 1; i >= 0; i--)
    {
	if(a.cyf[i] < b.cyf[i])
	    return true;
	if(a.cyf[i] > b.cyf[i])
	    return false;
    }
    return false;
}

bool mniej(Licz &a, Licz &b)
{
    if(a.znak && !b.znak)
	return true;
    if(!a.znak && b.znak)
	return false;
    if(a.znak && b.znak)
	return a.cyf != b.cyf && mniej_bezw(b, a);
    return mniej_bezw(a, b);
}

void dodaj(Licz &a, Licz &b, Licz &c)
{
    c.znak = false;
    if(!a.znak && !b.znak)
    {
	dodaj_dod(a, b, c);
	return;
    }
    if(a.znak && !b.znak)
    {
	if(mniej_bezw(b, a))
	{
	    odejmij_dod(a, b, c);
	    c.znak = true;
	    return;
	}
	odejmij_dod(b, a, c);
	return;
    }
    if(!a.znak && b.znak)
    {
	if(mniej_bezw(a, b))
	{
	    odejmij_dod(b, a, c);
	    c.znak = true;
	    return;
	}
	odejmij_dod(a, b, c);
	return;
    }
    dodaj_dod(a, b, c);
    c.znak = true;
}

void odejmij(Licz &a, Licz &b, Licz &c)
{
    b.znak = !b.znak;
    dodaj(a, b, c);
    b.znak = !b.znak;
}

void dodaj_dod(Licz &a, Licz &b, Licz &c)
{
    c.cyf = "";
    c.dl = 0;
    char dziel = 0;
    int min_dl = min(a.dl, b.dl);
    for(int i = 0; i < min_dl; i++)
    {
	c.cyf += (a.cyf[i] + b.cyf[i] + dziel) % 10;
	dziel = (a.cyf[i] + b.cyf[i] + dziel) / 10;
	c.dl++;
    }
    if(a.dl > b.dl)
    {
	for(int i = min_dl; i < a.dl; i++)
	{
	    c.cyf += (a.cyf[i] + dziel) % 10;
	    dziel = (a.cyf[i] + dziel) / 10;
	    c.dl++;
	}
    }
    else
    {
	for(int i = min_dl; i < b.dl; i++)
	{
	    c.cyf += (b.cyf[i] + dziel) % 10;
	    dziel = (b.cyf[i] + dziel) / 10;
	    c.dl++;
	}
    }
    if(dziel != 0)
    {
	c.cyf += dziel;
	c.dl++;
    }
}

void odejmij_dod(Licz &a, Licz &b, Licz &c)
{
    if(a.dl == b.dl)
	if(a.cyf == b.cyf)
	{
	    c.cyf = "\0";
	    c.dl = 1;
	    c.znak = 0;
	    return;
	}
    char przen = 0;
    int cyfra;
    c.dl = 1;
    for(int i = 0; i < a.dl; i++)
    {
	if(i < b.dl)
	    cyfra = a.cyf[i] - b.cyf[i] - przen;
	else
	    cyfra = a.cyf[i] - przen;
	przen = 0;
        if(cyfra < 0)
        {
	    przen++;
	    cyfra += 10;
        }
	if(cyfra != 0)
	    c.dl = i + 1;
        c.cyf += static_cast <char> (cyfra);
    }
}

Licz::Licz(string w)
{
    int il0 = 0;
    if(w.size() == 0)
    {
	cyf = "\0";
	dl = 1;
	znak = 0;
	return;
    }
    if(w[0] == '-')
    {
	il0 = 1;
	znak = 1;
    }
    else
	znak = 0;
    for(; il0 < w.size() && w[il0] == '0'; il0++)
    {
	if(il0 == w.size() - 1)
	{
	    cyf = "\0";
	    dl = 1;
	    znak = 0;
	    return;
	}
    }
    cyf = "";
    dl = 0;
    for(int i = w.size() - 1; i >= il0; i--)
    {
	if(w[i] > '9' || w[i] < '0')
	{
	    cerr << "Nieprawidlowy znak (" << w[i] << ")\n";
	    exit(1);
	}
	cyf += w[i] - '0';
	dl++;
    }
}
